# README #

Content: 
	This repository contains the code for a siete y medio game, which is similar to Blackjack/21. 

cards.h
	- Contains the interface for Card, Hand, and Player classes. 
	
cards.cpp
	- Contains the implementations of the Card, Hand, and Player classes. 
	
	- An object of the Card class represents a card in a 'Spanish baraja' card deck, where a Card has one of four suits
	(oros - coins; bastos - clubs; espadas - spades; or copas - cups) and one of ten ranks (1-7, sota - jack; caballo - knight, 
	or rey - king). 
	
	- To represent drawing a random card from a deck, the Constructor of a Card object uses a random number generator to 
	generate both the rank and the suit; while this could technically generate the same card twice in a single game or round, 
	this is fine since games tend to be played with several decks simultaneously. 
	
	- The Card class also contains member functions that allow you to retrieve the rank/suit of the Card both in Spanish and 
	English, to get a numerical value for the Card's rank, and to compare two Cards. 
	
	- An object of the Hand class contains a vector of Card objects, and represents the hand that a Player has. 
	
	- The Hand class has a member function printHand() that prints out both the Spanish and English names of all the Cards 
	in the hand. It also has a member functon addCard(), which adds a new Card to the Hand and sorts the vector based on 
	rank afterwards.
	
	- An object of the Player class represents a player in the game. A Player object only keeps track of the amount of money 
	that the player has, and can update it through the member function update_money. There is also another member function 
	get_money(), which allows the program to retrieve how much money a particular Player has. 
	
siete_y_medio.cpp
	- Contains the code to run a game of siete y medio. 
	
	- Player objects are created to represent the user and the Dealer; the game is over when either of these Player objects 
	have 0 money. The user starts with 100 dollars and the Dealer starts off with 900, so the user loses if he/she ever has 
	0 dollars, and the Dealer loses if he/she loses 900 dollars. To represent this, at the end of each round in the game, 
	if the user wins, the user's Player object's money is increased, but the Dealer's Player object's money is never increased, 
	only decreased. 
	
	- In each round, the user makes a bet, which must be less than or equal to the amount of money that the user has. Then 
	Card objects are added to the user's Hand until the user's total is greater than 7.5 or until the user says he/she does 
	not want any more Cards. The Dealer then is given a set of Card objects; the Dealer always containues to draw Cards until 
	the Dealer's total is greater than 5.5. 
	
	- After both the user and the Dealer have received their Cards, the outcome of the round is determined. If 
	both the user and the Dealer have a total value of Cards that is greater than 7.5, then we say that both of them bust. 
	In this case, the Dealer wins; this is called house advantage. The Dealer also wins if the Dealer's total is closer to 7.5
	than the user's total is, and similarly, the user wins if the user's total is closer to 7.5 than the Dealer's is. 
	If both the user and the Dealer have totals less than 7.5 which are equal, then no one wins or loses the round. 
	
	- If the user wins the round, then the user's money is increased by how much the user bet and the Dealer's money is 
	decreased by the same amount. If the Dealer wins the round, then the user's money is decreased by how much the user bet 
	but the Dealer's money is unchanged. If neither wins, then no change is made to either the user's or the Dealer's money. 
	
	- At the end, the user wins if the Dealer reaches 0 money and the Dealer wins if the user reaches 0 money. 	
	
	