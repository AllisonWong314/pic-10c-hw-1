#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

int main() {
	//Set the seed for the random number generator that is called in the Card constructor
	srand(time(NULL)); 

	//Create a Player object to represent the user of this program; the Player starts off with 100 dollars
	Player you = Player(100); 
	
	//Create a Player object to represent the Dealer; the Dealer will have 900 dollars
	Player Dealer = Player(900); 
	
	while(you.get_money()> 0 && Dealer.get_money() > 0) { //keep playing 
		
		//Create a Hand object to represent the Cards in the user's hand
		Hand your_Hand(); 
		//Create a variable that will keep track of the user's total 
		int your_total = 0; 
	
		//Create a Hand object to represent the Cards in the Dealer's hand
		Hand Dealer_Hand(); 
		//Create a variable that will keep track of the Dealer's total
		int Dealer_total = 0; 
	
		std::cout << "You have $" << you.get_money() << ". Enter a bet: ";
		int user_bet = 0; 
		std::cin >> user_bet; 
		
		// Make sure that the user does not bet more money than he/she has
		while(user_bet > you.get_money()) {
			std::cout << "You do not have enough money to place that bet. Please enter another bet.";
			std::cin >> user_bet; 
		}
		
		// Will determine whether or not to give the user another Card
		std::string another_card = "y"
		
		while(another_card == "y" && your_total <= 7.5) {
			// Give the user a card. 
			Card new_card = Card(); 
			
			// Print out what the new Card is. 
			std::string SpanishName = new_card.get_spanish_rank() + " de " + new_card.get_spanish_suit(); 
			std::string EnglishName = new_card.get_english_rank() + " of " + new_card.get_english_suit(); 
			std::cout << "     " << setwidth(22) << left << SpanishName << "(" << EnglishName << ")." << std::endl; 
		
			// Add the new card to the user's Hand
			your_Hand.addCard(new_card);
		
			// Update the user's total 
			double card_value = new_card.get_rank(); 
			if (card_value >= 10) {
				card_value = 0.5;
			}
			your_total += card_value; 
		
			// Print out the user's cards so that they can decide whether or not to add another card
			std::cout << "Your cards: " << std::endl; 
			your_Hand.printHand(); 
			std::cout << "Your total is " << your_total << ". Do you want another card (y/n)?" 
			std::cin >> another_card; 
		}
		
		while(Dealer_total <= 5.5) { // The Dealer always draws cards if his/her total is less than or equal to 5.5
			// Give the Dealer a new card. 
			Card new_card = Card(); 
			
			// Print out what the new Card is. 
			std::string SpanishName = new_card.get_spanish_rank() + " de " + new_card.get_spanish_suit(); 
			std::string EnglishName = new_card.get_english_rank() + " of " + new_card.get_english_suit(); 
			std::cout << "     " << setwidth(22) << left << SpanishName << "(" << EnglishName << ")." << std::endl; 
		
			// Add the new card to the user's Hand
			Dealer_Hand.addCard(new_card);
		
			// Update the user's total 
			double card_value = new_card.get_rank(); 
			if (card_value >= 10) {
				card_value = 0.5;
			}
			Dealer_total += card_value; 
		
			// Print out the user's cards so that they can decide whether or not to add another card
			std::cout << "Dealer's cards: " << std::endl; 
			Dealer_Hand.printHand(); 
			std::cout << "The Dealer's total is " << Dealer_total << "." 
		}
		
		// Determine who wins the round. 
		if( your_total > 7.5 || (your_total < Dealer_total && Dealer_total <= 7.5) {
			std::cout << "Too bad. You lose " << user_bet << "." << std::endl; 
			you.update_money(-1 * user_bet); 
		}
		else if (Dealer_total < your_total && your_total <= 7.5) {
			std::cout << "You win " << user_bet << "." << std::endll; 
			you.update_money(user_bet); 
			Dealer.update_money(-1 * user_bet); 
		}
		else { // user and Dealer have same total, which is less than or equal to 7.5; no one wins 
			std::cout << "Nobody wins." << std::endl; 
		}
	}
	
	// Determine who won the game. 
	if (you.get_money() == 0) {
		std::cout << "You have $0. GAME OVER!" << std::endl; 
		std::cout << "Come back when you have more money." << std::endl; << std::endl;
		std::cout << "Bye!" << std::endl; 
		return 0; 
	}
	else { // Dealer has $0, meaning that the Dealer has lost 900 dollars and loses 
		std::cout << "Congratulations! You beat the casino!" << std::endl; << std::endl; 
		std::cout << "Bye!" << std::endl;
		return 0; 
	}
}