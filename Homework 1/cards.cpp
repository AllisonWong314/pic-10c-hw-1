#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <iomanip> 


/* *************************************************
Card class
************************************************* */

/*
Default constructor for the Card class. Assigns a random suit and rank to create a Card object. 
Note that this can give repeated cards; this is OK since most variations of Blackjack are played with
several decks of cards at the same time.
*/
Card::Card() {
	int r = 1 + rand() % 4;
	switch (r) {
	case 1: suit = OROS; break;
	case 2: suit = COPAS; break;
	case 3: suit = ESPADAS; break;
	case 4: suit = BASTOS; break;
	default: break;
	}

	r = 1 + rand() % 10;
	switch (r) {
	case  1: rank = AS; break;
	case  2: rank = DOS; break;
	case  3: rank = TRES; break;
	case  4: rank = CUATRO; break;
	case  5: rank = CINCO; break;
	case  6: rank = SEIS; break;
	case  7: rank = SIETE; break;
	case  8: rank = SOTA; break;
	case  9: rank = CABALLO; break;
	case 10: rank = REY; break;
	default: break;
	}
}

// Accessor: returns a string with the suit of the card in Spanish 
string Card::get_spanish_suit() const {
	string suitName;
	switch (suit) {
	case OROS:
		suitName = "oros";
		break;
	case COPAS:
		suitName = "copas";
		break;
	case ESPADAS:
		suitName = "espadas";
		break;
	case BASTOS:
		suitName = "bastos";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
string Card::get_spanish_rank() const {
	string rankName;
	switch (rank) {
	case AS:
		rankName = "As";
		break;
	case DOS:
		rankName = "Dos";
		break;
	case TRES:
		rankName = "Tres";
		break;
	case CUATRO:
		rankName = "Cuatro";
		break;
	case CINCO:
		rankName = "Cinco";
		break;
	case SEIS:
		rankName = "Seis";
		break;
	case SIETE:
		rankName = "Siete";
		break;
	case SOTA:
		rankName = "Sota";
		break;
	case CABALLO:
		rankName = "Caballo";
		break;
	case REY:
		rankName = "Rey";
		break;
	default: break;
	}
	return rankName;
}



// Accessor: returns a string with the suit of the card in English 
string Card::get_english_suit() const {
	string ENsuitName; 
	switch (suit) {
		case OROS: 
			ENsuitName = "coins"; 
			break; 
		case COPAS: 
			ENsuitName = "cups"; 
			break;
		case ESPADAS: 
			ENsuitName = "spades";
			break;
		case BASTOS:
			ENsuitName = "clubs"; 
			break;
		default: break;
	}
	return ENsuitName;
}

// Accessor: returns a string with the rank of the card in English 
string Card::get_english_rank() const {
	string ENrankName;
	switch (rank) {
		case AS:
			ENrankName = "Ace";
			break;
		case DOS:
			ENrankName = "Two";
			break;
		case TRES:
			ENrankName = "Three";
			break;
		case CUATRO:
			ENrankName = "Four";
			break;
		case CINCO:
			ENrankName = "Five";
			break;
		case SEIS:
			ENrankName = "Six";
			break;
		case SIETE:
			ENrankName = "Seven";
			break;
		case SOTA:
			ENrankName = "Jack";
			break;
		case CABALLO:
			ENrankName = "Knight";
			break;
		case REY:
			ENrankName = "King";
			break;
		default: break;
	}
	return ENrankName;
}



// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
	return static_cast<int>(rank) + 1;
}

// Comparison operator for cards; returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
	return rank < card2.rank;
}



/* *************************************************
Hand class
************************************************* */

//Constructor - default constructs a vector of Cards
Hand::Hand(): hand(std::vector<Card>()) {}

// Accessor - prints out the Cards (both in English and Spanish) in the Hand
void Hand::printHand() const {
	for(size_t i = 0; i < hand.size(); ++i) {
		std::string SpanishName = hand[i].get_spanish_rank() + " de " + hand[i].get_spanish_suit(); 
		std::string EnglishName = hand[i].get_english_rank() + " of " + hand[i].get_english_suit(); 
		std::cout << "     " << setwidth(22) << left << SpanishName << "(" << EnglishName << ")." << std::endl; 
	}
	return; 
}

// Mutator - adds a Card object to the hand member variable and then re-sorts the vector
	void addCard(const Card& card_to_add) {
		hand.push_back(card_to_add);
		
		// Sorts the vector of Cards so that they appear in order of rank.
		// We can do this because we have operator< defined for Card objects 
		std::sort(hand.begin(), hand.end());  
	}

private:
	std::vector<Card> hand; 
};



/* *************************************************
Player class
************************************************* */

// Constructor - takes in an int and sets the Player's money member variable to that int. 
Player::Player(int m) : money(m) {}

// Accessor - allows you to retrieve the amount of money a Player object has. 
int Player::get_money() const {
	return money; 
}
	
// Mutator - accepts an int (can be a negative or positive amount of money) and updates the Player's money accordingly. 
void Player::update_money(int change_in_money) {
	money += change_in_money; 
	return; 
}
