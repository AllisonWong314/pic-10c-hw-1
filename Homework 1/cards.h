#include <string>
#include <vector>
#include <fstream>

#ifndef CARDS_H
#define CARDS_H

using namespace std;

enum suit_t { OROS, COPAS, ESPADAS, BASTOS };

/*
The values for this type start at 0 and increase by one until they get to SIETE.
The rank reported by the function Card::get_rank() below is the value listed here plus one.
E.g:
The rank of AS is reported as    static_cast<int>(AS) + 1   = 0 + 1 =  1
The rank of SOTA is reported as  static_cast<int>(SOTA) + 1 = 9 + 1 = 10
*/
enum rank_t { AS, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA = 9, CABALLO = 10, REY = 11 };

class Card {
public:
	// Constructor assigns random rank & suit to card.
	Card();

	// Accessors 
	string get_spanish_suit() const;
	string get_spanish_rank() const;

	string get_english_suit() const;
	string get_english_rank() const;

	// Converts card rank to number.
	// The possible returns are: 1, 2, 3, 4, 5, 6, 7, 10, 11 and 12
	int get_rank() const;

	// Compare rank of two cards. E.g: Eight < Jack is true.
	// Assume Ace is always 1. 
	// Useful if you want to sort the cards.
	bool operator < (Card card2) const;

private:
	suit_t suit;
	rank_t rank;
};


class Hand {
public:
	// A vector of Cards
	Hand();

	// Accessor - prints out the Cards (both in English and Spanish) in the Hand
	void printHand() const; 
	
	// Mutator - adds a Card object to the hand member variable 
	void addCard(const Card& card_to_add); 

private:
	std::vector<Card> hand; 
};


class Player {
public:
	// Constructor. Assigns initial amount of money
	Player(int m);

	// Accessor - allows you to retrieve the amount of money a Player object has. 
	int get_money() const; 
	
	// Mutator - accepts an int (can be a negative or positive amount of money) and updates the Player's money accordingly. 
	void update_money(int change_in_money);

private:
	int money;
	
};

#endif
